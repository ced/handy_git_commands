#### Cloning a repository from a local source
```
git clone file:///home/cedric/src/github.com/btcsuite/btcwallet/.git /home/cedric/src/github.com/colakong/cedwallet
```

##### Show head commit
`git rev-parse HEAD`

##### Revert a file
`git checkout <file>`

##### Adding a remote
For a new repo created locally, to push contents to remote branch
```
git remote -v
git remote add origin git@gitlab.com/ced/someproject.git
git remote -v
```
So that you can pull changes from remote
```
# First-time can use -u flag to set remote as upstream for pull/status
git push -u origin

# Or you can specify its name, say if you had a mirror elsewhere
git remote add github git@gitlab.com/colakong/someproject.git
git push github
```

##### Merging forks from github

###### (One-time only) Add the upstream repo to my list of remote repos in my existing git repo
```
git remote add <name> <url>
git remote add upstream git@github.com:someoneelse/someproject.git
```

###### Fetch the changes from upstream
`git fetch upstream`

###### Merge changes
`git merge upstream/master`

##### Delete last local commit
`git reset --soft HEAD~1`

##### Undo pending changes
`git reset HEAD <file>`

##### Show pending changes between local repo checkout and remote origin
```
git diff --stat --cached origin/<branch>
git diff --stat --cached origin/staging-v1
```

##### Create a new branch from an unstaged set of diffs
```
git stash list
git stash save "ticket123 - add recursive cleanup thingy"
git stash list
git stash branch recurseClean <stash>
git commit -a -m "ticket123 - add recursive cleanup thingy to thisAndThat module"
# Push new branch to remote 'origin' repo
git push origin recurseClean
```

#### Create package from git repo checkout
`git archive --format=tar master | gzip > ~/tmp/banana-0.2.5.tar.gz`

##### Using a branch

###### Creating a branch
`git checkout -b [name_of_your_new_branch]`

###### Push the branch to github
`git push [name_of_remote] [name_of_your_new_branch]`

If the branch should have a different name on the remote end than it does locally, the form is:
`git push [name_of_remote] [local_name]:[remote_name]`

###### Show branches
`git branch`

###### Update branch with code from original, upstream branch
```
git fetch [name_of_remote]
git merge [name_of_remote]/[name_of_remote_upstream_branch]
```

###### Make changes
```
git commit -a -m "Updated my branchy branch"
git push
```

###### Delete local branch
`git branch -d [name_of_your_new_branch]`

###### Delete branch on github/gitlab/remote git repo
`git push origin :[name_of_new_branch]`

or

`git push --delete origin [name_of_new_branch]`

###### Copy a branch
`git checkout -b new_branch old_branch`

##### Set a file as executable
`git update-index --chmod=+x`

##### Apply the changes from a commit to existing files:
`git diff HEAD 4e42411ed5ee9d17f9c28ba90fb309623fdabb92 | git apply`

##### Show commits where files were deleted
`git log --diff-filter=D --summary`

##### Restore a file from a commit before it was deleted
`git checkout <commit>~1 <filename>`
